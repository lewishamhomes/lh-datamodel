//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class InspectionOutcome
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InspectionOutcome()
        {
            this.RepairOrAdaptations = new HashSet<RepairOrAdaptation>();
        }
    
        public System.Guid InspectionId { get; set; }
        public string GiftedItems { get; set; }
    
        public virtual VoidInspectionInstruction VoidInspectionInstruction { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RepairOrAdaptation> RepairOrAdaptations { get; set; }
    }
}
