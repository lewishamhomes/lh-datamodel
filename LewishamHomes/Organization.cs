//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class Organization
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Organization()
        {
            this.CreditReferences = new HashSet<CreditReference>();
            this.DetailedTenancyApplicationForms = new HashSet<DetailedTenancyApplicationForm>();
            this.Engagements = new HashSet<Engagement>();
            this.NeedsAssessmentOrRecommendations = new HashSet<NeedsAssessmentOrRecommendation>();
            this.Organization1 = new HashSet<Organization>();
            this.SocialMediaHandles = new HashSet<SocialMediaHandle>();
            this.SupplierReferences = new HashSet<SupplierReference>();
            this.PreviousTenancies = new HashSet<PreviousTenancy>();
            this.Properties = new HashSet<Property>();
            this.ProspectiveTenants = new HashSet<ProspectiveTenant>();
            this.Agreements = new HashSet<Agreement>();
            this.People = new HashSet<Person>();
            this.Agreements1 = new HashSet<Agreement>();
            this.Parties = new HashSet<Party>();
        }
    
        public System.Guid Id { get; set; }
        public bool ApprovedForSharing { get; set; }
        public string Name { get; set; }
        public int ShortName { get; set; }
        public string Description { get; set; }
        public string DoingBusinessAs { get; set; }
        public string CompanyRegistrationNumber { get; set; }
        public string PlaceOfRegistration { get; set; }
        public string SICCode { get; set; }
        public string SalesTaxRegistrationNumber { get; set; }
        public string Website { get; set; }
        public System.DateTime IncorporatedDate { get; set; }
        public bool RegisteredForSalesTaxIndicator { get; set; }
        public System.Guid RegisteredOfficeAddress { get; set; }
        public System.Guid PostalAddress { get; set; }
        public string GeographyServed { get; set; }
        public string InsuranceDetails { get; set; }
        public string ProcurementHistory { get; set; }
        public string Reference { get; set; }
        public string ServicesOffered { get; set; }
        public string Type { get; set; }
        public Nullable<System.Guid> ParentOrganization { get; set; }
        public Nullable<int> LegalClassification { get; set; }
    
        public virtual Bank Bank { get; set; }
        public virtual Contractor Contractor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditReference> CreditReferences { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailedTenancyApplicationForm> DetailedTenancyApplicationForms { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Engagement> Engagements { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NeedsAssessmentOrRecommendation> NeedsAssessmentOrRecommendations { get; set; }
        public virtual NominatingAuthority NominatingAuthority { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Organization> Organization1 { get; set; }
        public virtual Organization Organization2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SocialMediaHandle> SocialMediaHandles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SupplierReference> SupplierReferences { get; set; }
        public virtual SupportingAgency SupportingAgency { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PreviousTenancy> PreviousTenancies { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Property> Properties { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProspectiveTenant> ProspectiveTenants { get; set; }
        public virtual Utility Utility { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Agreement> Agreements { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Person> People { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Agreement> Agreements1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Party> Parties { get; set; }
    }
}
