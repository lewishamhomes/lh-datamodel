//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class SupportingAgency
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SupportingAgency()
        {
            this.CareTeams = new HashSet<CareTeam>();
        }
    
        public System.Guid OrganizationId { get; set; }
        public string DataSharingAgreement { get; set; }
        public int RelationshipType { get; set; }
        public string SocialServices { get; set; }
        public string SupportProvided { get; set; }
        public string TypeOfOrganisation { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CareTeam> CareTeams { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
