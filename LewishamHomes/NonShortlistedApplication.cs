//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class NonShortlistedApplication
    {
        public System.Guid ShortlistId { get; set; }
        public System.Guid ApplicationId { get; set; }
        public string Reason { get; set; }
    
        public virtual Application Application { get; set; }
        public virtual Shortlist Shortlist { get; set; }
    }
}
