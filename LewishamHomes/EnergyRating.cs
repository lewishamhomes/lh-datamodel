//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class EnergyRating
    {
        public System.Guid UnitId { get; set; }
        public string Category { get; set; }
        public string OrganizationName { get; set; }
        public decimal Index { get; set; }
    
        public virtual Unit Unit { get; set; }
    }
}
