//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServiceCharge
    {
        public System.Guid Id { get; set; }
        public System.Guid LeaseId { get; set; }
        public int TypeCode { get; set; }
        public string TypeDescription { get; set; }
        public string ServiceChargeYearStart { get; set; }
        public string ServiceChargeYearEnds { get; set; }
        public bool BudgetRequired { get; set; }
        public bool ExceptionalExpenditure { get; set; }
        public bool PaidOnAccount { get; set; }
        public bool RepairOfStructure { get; set; }
        public bool ReserveOrSinkingFund { get; set; }
    
        public virtual Lease Lease { get; set; }
    }
}
