//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class BuildingInformation
    {
        public System.Guid PropertyId { get; set; }
        public System.DateTime ConstructionDate { get; set; }
    
        public virtual Property Property { get; set; }
    }
}
