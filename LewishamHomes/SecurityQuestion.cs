//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class SecurityQuestion
    {
        public System.Guid Id { get; set; }
        public System.Guid PersonId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    
        public virtual Person Person { get; set; }
    }
}
