//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LewishamHomes
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompletedAssignment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompletedAssignment()
        {
            this.RentSecurityDeposits = new HashSet<RentSecurityDeposit>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid LeaseId { get; set; }
        public int WholeOrPartCode { get; set; }
        public System.Guid AssigneeParty { get; set; }
        public System.Guid AssignorParty { get; set; }
        public System.DateTime CompletionDate { get; set; }
    
        public virtual Party Party { get; set; }
        public virtual Party Party1 { get; set; }
        public virtual Lease Lease { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RentSecurityDeposit> RentSecurityDeposits { get; set; }
    }
}
