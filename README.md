This repository contains a data model for a social housing CRM system, that
implements the HACT UK housing data standard.
(https://www.hact.org.uk/DataStandard)

The intention of the project is to create a "headless CRM" wrapped in APIs
on which organisations can build their own products.

Lewisham Homes will build these APIS out following the White House API Standard
(https://github.com/WhiteHouse/api-standards) as we develop various digital
products for our resident-facing teams.